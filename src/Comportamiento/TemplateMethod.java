package Comportamiento;

//  https://danielggarcia.wordpress.com/2014/05/05/patrones-de-comportamiento-iii-template-method/
/*
    Permitir que ciertos pasos de un algoritmo definidos en un método de una clase
    sean redefinidos en sus clases derivadas sin necesidad de sobrecargar la operación entera
 */

abstract class Protocolo {

    protected String nombre;

    //  METODO PUBLICO EJECUTADO DESDE EL CLIENTE
    public void solicitar(String datos) {
        this.establecerProtocolo();
        this.validar();
        this.consultar(datos);
    }

    //  METODO REDEFINIDO POR LAS DEPENDENCIAS
    protected abstract void establecerProtocolo();

    //  METODO INTERNO
    protected void validar() {
        System.out.println("Validando conexion....");
    }

    //  METODO INTERNO
    protected void consultar(String datos) {
        System.out.println("Datos Consultados : " + datos);
    }
}

//  ABSTRACCIONES

class Socket extends Protocolo {

    @Override
    protected void establecerProtocolo() {
        System.out.println("Estableciendo Conexion Por Socket");
    }
}

class HTTP extends  Protocolo {

    @Override
    protected void establecerProtocolo() {
        System.out.println("Estableciendo Conexion Por HTTP");
    }
}

class TemplateMethod {
    public static void main(String args[]) {
        System.out.println("\nTemplate Method\n");

        Protocolo socket = new Socket();
        socket.solicitar("LOREM IPSUM");

        System.out.println("\n");

        Protocolo http = new HTTP();
        http.solicitar("LOREM IPSUM");
    }
}
