package Comportamiento;

//  https://danielggarcia.wordpress.com/2014/05/20/patrones-de-comportamiento-v-patron-state/
//  http://migranitodejava.blogspot.com/2011/06/state.html

//  STATE

interface IState {
    public void acelerar();
    public void frenar();
    public void encenderApagar(); //    SWITCH LLAVES
}

//  CONTEXT

class Vehiculo {

    private IState iState;
    private double velocidadActual;
    private double combustibleActual;

    public Vehiculo(int combustibleActual) {
        this.combustibleActual = combustibleActual;
        iState = new ApagadoState(this); // ESTADO POR DEFECTO
    }

    //  STATE FUNCTIONS

    public void acelerar() {
        iState.acelerar();
        //  System.out.println("Velocidad Actual y Combustible Actual : " + velocidadActual + ", " + combustibleActual);
    }

    public void frenar() {
        iState.frenar();
    }

    public void encenderApagar() {
        iState.encenderApagar();
    }

    public void obtenerInformacion() {
        System.out.println("Velocidad Actual y Combustible Actual : " + velocidadActual + ", " + combustibleActual);
    }

    //  CONTEXT FUNCTIONS

    public void modificarVelocidad(int velocidadActual) {
        this.velocidadActual += velocidadActual;
    }

    public void modificarCombustible(int combustibleActual) {
        this.combustibleActual += combustibleActual;
    }

    //  GETTERS AND SETTERS

    public IState getiState() {
        return iState;
    }

    public void setiState(IState iState) {
        this.iState = iState;
    }

    public double getVelocidadActual() {
        return velocidadActual;
    }

    public void setVelocidadActual(double velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    public double getCombustibleActual() {
        return combustibleActual;
    }

    public void setCombustibleActual(double combustibleActual) {
        this.combustibleActual = combustibleActual;
    }
}

//  CONCRETS STATES

class ApagadoState implements IState {

    private Vehiculo vehiculo;

    public ApagadoState(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public void acelerar() {
        System.out.println("Error: Vehiculo Apagado. Encienda El Carro Para Iniciar");
    }

    @Override
    public void frenar() {
        System.out.println("Error: Vehiculo Apagado. Encienda El Carro Para Iniciar");
    }

    @Override
    public void encenderApagar() {

        if(vehiculo.getCombustibleActual() > 0) {
            vehiculo.setiState(new DetenidoState(vehiculo));
            System.out.println("Vehiculo Detenido Listo Para Iniciar");
            vehiculo.modificarVelocidad(0);
        }
        else {
            vehiculo.setiState(new SinCombustibleState(vehiculo));
            System.out.println("Error: Vehiculo Sin Combustible");
        }

    }
}

class DetenidoState implements IState {

    private Vehiculo vehiculo;

    public DetenidoState(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public void acelerar() {

        if(vehiculo.getCombustibleActual() > 0) {
            vehiculo.setiState(new EnMarchaState(vehiculo));
            System.out.println("El Vehiculo Esta En Marcha");
            vehiculo.modificarVelocidad(10);
            vehiculo.modificarCombustible(-10);

            vehiculo.obtenerInformacion();
        }
        else {
            vehiculo.setiState(new SinCombustibleState(vehiculo));
            System.out.println("Error: Vehiculo Sin Combustible");
        }

    }

    @Override
    public void frenar() {
        System.out.println("Error: Vehiculo Esta Detenido");
    }

    @Override
    public void encenderApagar() {
        vehiculo.setiState(new ApagadoState(vehiculo));
        System.out.println("El Vehiculo Esta Apagado");
    }
}

class EnMarchaState implements IState {

    private Vehiculo vehiculo;
    private static final int VELOCIDAD_MAXIMA  = 180;

    public EnMarchaState(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public void acelerar() {

        if(vehiculo.getCombustibleActual() > 0) {
            if(vehiculo.getVelocidadActual() >= VELOCIDAD_MAXIMA) {
                vehiculo.modificarVelocidad(-10);
                System.out.println("Error: Vehiculo Alcanzando Su Velocidad Máxima");
            }
            else {
                vehiculo.modificarVelocidad(10);
                vehiculo.modificarCombustible(-10);
            }

            vehiculo.obtenerInformacion();
        }
        else {
            vehiculo.setiState(new SinCombustibleState(vehiculo));
            System.out.println("Error: Vehiculo Sin Combustible");
        }

    }

    @Override
    public void frenar() {

        vehiculo.modificarVelocidad(-10);
        vehiculo.obtenerInformacion();

        if(vehiculo.getVelocidadActual() <= 0) {
            vehiculo.setiState(new DetenidoState(vehiculo));
            System.out.println("El Vehiculo Esta Detenido");
        }

    }

    @Override
    public void encenderApagar() {
        System.out.println("Error: Vehiculo En Marcha");
    }
}

class SinCombustibleState implements IState {

    private Vehiculo vehiculo;

    public SinCombustibleState(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public void acelerar() {
        System.out.println("Error: Vehiculo Sin Combustible");
    }

    @Override
    public void frenar() {
        System.out.println("Error: Vehiculo Sin Combustible");
    }

    @Override
    public void encenderApagar() {
        System.out.println("Advertencia: Vehiculo Sin Combustible");
    }
}

class State {
    public static void main(String args[]) {
        System.out.println("\nPattern State\n");

        Vehiculo vehiculo = new Vehiculo(40);

        vehiculo.acelerar();
        vehiculo.encenderApagar();
        vehiculo.acelerar();
        vehiculo.acelerar();
        vehiculo.acelerar();
        vehiculo.frenar();
        vehiculo.frenar();
        vehiculo.frenar();
    }
}
