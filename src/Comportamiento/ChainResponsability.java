package Comportamiento;

//  http://migranitodejava.blogspot.com/search/label/Chain%20of%20Responsibility

interface IAprobador {
    public void setNext(IAprobador iAprobador);
    public IAprobador getNext();
    public void aprobar(int monto);
}

class Oficial implements IAprobador {

    private IAprobador next;

    @Override
    public void setNext(IAprobador next) {
        this.next = next;
    }

    @Override
    public IAprobador getNext() {
        return next;
    }

    @Override
    public void aprobar(int monto) {
        if(monto <= 10000) {
            System.out.println("Prestamo de " + monto + " Aprobado Por Oficial");
        }
        else {
            next.aprobar(monto);
        }
    }
}

class Gerente implements IAprobador {

    private IAprobador next;

    @Override
    public void setNext(IAprobador next) {
        this.next = next;
    }

    @Override
    public IAprobador getNext() {
        return next;
    }

    @Override
    public void aprobar(int monto) {
        if(monto > 10000 && monto <= 100000) {
            System.out.println("Prestamo de " + monto + " Aprobado Por Gerente");
        }
        else {
            next.aprobar(monto);
        }
    }
}

class Director implements IAprobador {

    private IAprobador next;

    @Override
    public void setNext(IAprobador next) {
        this.next = next;
    }

    @Override
    public IAprobador getNext() {
        return next;
    }

    @Override
    public void aprobar(int monto) {
        if(monto > 100000) {
            System.out.println("Prestamo de " + monto + " Aprobado Por Director");
        }
        else {
            next.aprobar(monto);
        }
    }
}

//  MANAGER

class Banco implements IAprobador {

    private IAprobador next;

    @Override
    public void setNext(IAprobador next) {
        this.next = next;
    }

    @Override
    public IAprobador getNext() {
        return next;
    }

    @Override
    public void aprobar(int monto) {

        Oficial oficial = new Oficial();
        this.setNext(oficial);

        Gerente gerente = new Gerente();
        oficial.setNext(gerente);

        Director director = new Director();
        gerente.setNext(director);

        next.aprobar(monto);

    }
}

class ChainResponsability {
    public static void main(String args[]) {

        System.out.println("\nPattern ChainResponsability\n");

        Banco banco = new Banco();

        banco.aprobar(25000);
        banco.aprobar(145000);
        banco.aprobar(6000);

    }
}
