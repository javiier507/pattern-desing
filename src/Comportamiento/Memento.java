package Comportamiento;

import java.util.*;

//  https://es.wikipedia.org/wiki/Memento_(patr%C3%B3n_de_dise%C3%B1o)

class MementoState
{
    private String state;

    public MementoState(String stateToSave)
    {
        state = stateToSave;
    }

    public String getSavedState()
    {
        return state;
    }
}

class Originator
{
    private String state;
    /* lots of memory consumptive private data that is not necessary to define the
     * state and should thus not be saved. Hence the small memento object. */

    public void set(String state)
    {
        System.out.println("Originator: Setting state to "+state);
        this.state = state;
    }

    public MementoState saveToMemento()
    {
        System.out.println("Originator: Saving to Memento.");
        return new MementoState(state);
    }

    public void restoreFromMemento(MementoState m)
    {
        state = m.getSavedState();
        System.out.println("Originator: State after restoring from Memento: "+state);
    }
}

class Caretaker {
    private ArrayList<MementoState> savedStates = new ArrayList<MementoState>();

    public void addMemento(MementoState m) {
        savedStates.add(m);
    }

    public MementoState getMemento(int index) {
        return savedStates.get(index);
    }
}

class Memento {
    public static void main(String args[]) {
        System.out.println("\nMemento\n");

        Caretaker caretaker = new Caretaker();

        Originator originator = new Originator();
        originator.set("State1");
        originator.set("State2");
        caretaker.addMemento( originator.saveToMemento() );
        originator.set("State3");
        caretaker.addMemento( originator.saveToMemento() );
        originator.set("State4");

        originator.restoreFromMemento( caretaker.getMemento(1) );
    }
}
