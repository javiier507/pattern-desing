package Comportamiento;

//  https://es.wikipedia.org/wiki/Command_(patr%C3%B3n_de_dise%C3%B1o)
/*
    Este patrón permite solicitar una operación a un objeto sin conocer
    realmente el contenido de esta operación, ni el receptor real de la misma.
    Para ello se encapsula la petición como un objeto, con lo que además facilita
    la parametrización de los métodos.
 */

//  CLASE RECEPTORA

class Luces {

    public Luces() {}

    public void encender() {
        System.out.println("Encender!");
    }

    public void apagar() {
        System.out.println("Apagar!");
    }
}

//  COMANDO

interface ILucesCommand {
    public void ejecutar();
}

//  COMANDOS CONCRETOS

class EncenderLuces implements ILucesCommand {

    private Luces luces;

    EncenderLuces(Luces luces) {
        this.luces = luces;
    }

    @Override
    public void ejecutar() {
        this.luces.encender();
    }
}

class ApagarLuces implements ILucesCommand {

    private Luces luces;

    ApagarLuces(Luces luces) {
        this.luces = luces;
    }

    @Override
    public void ejecutar() {
        this.luces.apagar();
    }
}

//  INVOCADORA O HANDLER

class Manejoluces {

    private ILucesCommand encederCommand;
    private ILucesCommand apagarCommand;

    Manejoluces(ILucesCommand encederCommand, ILucesCommand apagarCommand) {
        this.encederCommand = encederCommand;
        this.apagarCommand = apagarCommand;
    }

    public void encender() {
        this.encederCommand.ejecutar();
    }

    public void apagar() {
        this.apagarCommand.ejecutar();
    }
}

class Command {
    public static void main(String args[]) {
        System.out.println("\nCommand\n");

        Luces luces = new Luces();

        ILucesCommand encenderCommand = new EncenderLuces(luces);
        ILucesCommand apagarCommand = new ApagarLuces(luces);

        Manejoluces handler = new Manejoluces(encenderCommand, apagarCommand);

        handler.encender();
        handler.apagar();
        handler.encender();
        handler.apagar();

    }
}
