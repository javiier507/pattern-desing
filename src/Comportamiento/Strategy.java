package Comportamiento;

//  https://es.wikipedia.org/wiki/Strategy_(patr%C3%B3n_de_dise%C3%B1o)
//  https://danielggarcia.wordpress.com/2014/05/12/patrones-de-comportamiento-iv-patron-strategy/

//  STRATEGY

interface IApi {
    public void respond(String data);
}

//  CONCRETE STRATEGIES

class JSON implements IApi {
    @Override
    public void respond(String data) {
        System.out.println("JSON : " + data);
    }
}

class XML implements IApi {
    @Override
    public void respond(String data) {
        System.out.println("XML : " + data);
    }
}

class Text implements IApi {
    @Override
    public void respond(String data) {
        System.out.println("Text : " + data);
    }
}

//  CONTEXT

class ApiContext {

    private IApi iApi;

    public ApiContext(IApi iApi) {
        this.iApi = iApi;
    }

    public void respond(String data) {
        iApi.respond(data);
    }

    //  GETTERS AND SETTERS

    public IApi getiApi() {
        return iApi;
    }

    public void setiApi(IApi iApi) {
        this.iApi = iApi;
    }
}

class Strategy {
    public static void main(String args[]) {
        System.out.println("\nPattern Strategy\n");

        String data = "Duis aute irure dolor in reprehenderit in voluptate "
                    + "velit esse cillum dolore eu fugiat nulla pariatur";

        ApiContext apiContext = new ApiContext(new JSON());
        apiContext.respond(data);

        apiContext = new ApiContext(new XML());
        apiContext.respond(data);

        apiContext.setiApi(new Text());
        apiContext.respond(data);
    }
}
