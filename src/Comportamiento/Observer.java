package Comportamiento;

//  http://migranitodejava.blogspot.com/2011/06/observer.html
//  https://danielggarcia.wordpress.com/2014/06/02/patrones-de-comportamiento-vi-patron-observer/

import java.util.ArrayList;
import java.util.Calendar;

//  OBSERVABLE

interface IObservable {
    public void agregar(IObservador iObservador);
    public void eliminar(IObservador iObservador);
    public void notificar(String mensaje);
}

//  OBSERVADOR

interface IObservador {
    public void actualizar(String mensaje);
}

//  OBSERVABLE CONCRETO

class Api implements IObservable {

    private ArrayList<IObservador> observadores = new ArrayList<IObservador>();

    @Override
    public void agregar(IObservador iObservador) {
        observadores.add(iObservador);
    }

    @Override
    public void eliminar(IObservador iObservador) {
        observadores.remove(iObservador);
    }

    @Override
    public void notificar(String mensaje) {
        for(int i=0; i<observadores.size(); i++) {
            observadores.get(i).actualizar(mensaje);
        }
    }
}

//  OBSERVADORES CONCRETOS

class Navegador implements IObservador {

    @Override
    public void actualizar(String mensaje) {
        System.out.println("Navegador, recibiendo mensaje : " + mensaje);
    }
}

class Movil implements IObservador {

    @Override
    public void actualizar(String mensaje) {
        System.out.println("Movil, recibiendo mensaje : " + mensaje);
    }
}

//  CLIENTE

class Cliente {

    private Api api = new Api();

    Cliente() {
        api.agregar(new Navegador());
        api.agregar(new Movil());
    }

    public void enviarInformacion() {

        try {

            for(int i=1; i<=100000; i++) {

                //  CADA 10K ITERACIONES, NOTIFICAR
                if(i % 10000 == 0) {

                    Thread.sleep(2000);
                    api.notificar(String.valueOf(i));
                    System.out.println("## " + String.valueOf(Calendar.getInstance().getTime()) + "\n");

                }
            }

        }catch (Exception ex){

        }
    }

}

class Observer {
    public static void main(String args[]) {

        System.out.println("\nPattern Observer\n");

        Cliente cliente = new Cliente();
        cliente.enviarInformacion();

    }
}
