package Creacionales;

//  http://migranitodejava.blogspot.com.co/2011/05/prototype.html
//  https://es.wikipedia.org/wiki/Prototipo_(patr%C3%B3n_de_dise%C3%B1o)

//  PROTOTIPO

import java.util.HashMap;

abstract class Tv implements Cloneable {

    private String marca;
    private int pulgada;
    private double precio;

    public Tv(String marca, int pulgada, double precio) {
        setMarca(marca);
        setPulgada(pulgada);
        setPrecio(precio);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString() {
        return "\nMarca : " + this.marca + "\nPulgada : " + this.pulgada + "\nPrecio : " + this.precio;
    }

    //  SETTERS AND GETTERS

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPulgada() {
        return pulgada;
    }

    public void setPulgada(int pulgada) {
        this.pulgada = pulgada;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}

//  PROTOTIPOS CONCRETOS

class Lcd extends Tv {

    Lcd(String marca, int pulgada, double precio) {
        super(marca, pulgada, precio);
    }
}

class Led extends Tv {

    Led(String marca, int pulgada, double precio) {
        super(marca, pulgada, precio);
    }
}

//  MANAGER

class Manager {

    private HashMap<String, Tv> prototipos = new HashMap<String, Tv>();

    public Manager() {
        Lcd lcd = new Lcd("Sony", 42, 419);
        Led led = new Led("Samsung", 42, 499);

        prototipos.put("LCD", lcd);
        prototipos.put("LED", led);
    }

    public Object obtener(String tipo) throws CloneNotSupportedException {
        Tv objeto = (Tv)prototipos.get(tipo);
        return objeto != null ? objeto.clone() : null;
    }
}

class Prototype {
    public static void main(String args[]) throws Exception {

        System.out.println("\nPrototype Pattern");

        Manager manager = new Manager();

        Tv lcd = (Tv) manager.obtener("LCD");
        if(lcd != null) {
            System.out.println("\nLCD " + lcd.toString());
        }

        Tv led = (Tv) manager.obtener("LED");
        if(led != null) {
            System.out.println("\nLED " + led.toString());
        }
    }
}
