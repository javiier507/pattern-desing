package Creacionales;

//  PRODUCTOS ABSTRACTOS

interface Dispositivo {
    String obtenerDescripcion();
}

interface Conexion {
    String conectar(Dispositivo dispositivo);
}

//  PRODUCTO CONCRETOS Dispositivo

class Celular implements Dispositivo {
    public String obtenerDescripcion() {
        return "Celular";
    }
}

class Tablet implements Dispositivo {
    public String obtenerDescripcion() {
        return "Tablet";
    }
}

//  PRODUCTO CONCRETOS Conexion

class Wifi implements Conexion {

    public String conectar(Dispositivo dispositivo) {
        return "Conectando " + dispositivo.obtenerDescripcion() + " a : Wifi";
    }

}

class Data implements Conexion {

    public String conectar(Dispositivo dispositivo) {
        return "Conectando " + dispositivo.obtenerDescripcion() + " a : Data Contrato";
    }
}

//  FABRICA ABSTRACTA

interface IFabricaAbstracta {
    Dispositivo crearDispositivo();
    Conexion crearConexion();
}

//  FABRICAS CONCRETAS

class FabricaCelularWifi implements IFabricaAbstracta {

    public Dispositivo crearDispositivo() {
        return new Celular();
    }

    public Conexion crearConexion() {
        return new Wifi();
    }

}

class FabricaCelularData implements IFabricaAbstracta {

    public Dispositivo crearDispositivo() {
        return new Celular();
    }

    public Conexion crearConexion() {
        return new Data();
    }

}

class FabricaTabletWifi implements IFabricaAbstracta {

    public Dispositivo crearDispositivo() {
        return new Tablet();
    }

    public Conexion crearConexion() {
        return new Wifi();
    }

}

class FabricaTabletData implements IFabricaAbstracta {

    public Dispositivo crearDispositivo() {
        return new Tablet();
    }

    public Conexion crearConexion() {
        return new Data();
    }

}

class FactoryAbstractFamily {
    public static void main(String args[]) {

        System.out.println("\nAbstract Factory Family\n");

        //  FABRICA
        IFabricaAbstracta fabrica = null;

        //  PRODUCTOS ABSTRACTOS
        Dispositivo celular = null, tablet = null;
        Conexion wifi = null, data = null;

        //  CELULAR + WIFI
        fabrica = new FabricaCelularWifi();
        celular = fabrica.crearDispositivo();
        wifi = fabrica.crearConexion();
        System.out.println(wifi.conectar(celular));

        //  CELULAR + DATA
        fabrica = new FabricaCelularData();
        celular = fabrica.crearDispositivo();
        data = fabrica.crearConexion();
        System.out.println(data.conectar(celular));

        //  TABLET + WIFI
        fabrica = new FabricaTabletWifi();
        tablet = fabrica.crearDispositivo();
        wifi = fabrica.crearConexion();
        System.out.println(wifi.conectar(tablet));

        //  TABLET + DATA
        fabrica = new FabricaTabletData();
        tablet = fabrica.crearDispositivo();
        data = fabrica.crearConexion();
        System.out.println(data.conectar(tablet));
    }
}
