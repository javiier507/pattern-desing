package Creacionales;

//  INTERFACES

interface ITransporte {
    public String solicitar();
}

interface ITransporteFabrica {
    public ITransporte crearTransporte();
}

//  PRODUCTOS

class Taxi implements ITransporte {
    public String solicitar() {
        return "Taxi Solicitado";
    }
}

class Uber implements ITransporte {
    public String solicitar() {
        return "Uber Solicitado";
    }
}

//  FABRICAS

class TaxiFabrica implements ITransporteFabrica {
    public ITransporte crearTransporte() {
        return new Taxi();
    }
}

class UberFabrica implements ITransporteFabrica {
    public ITransporte crearTransporte() {
        return new Uber();
    }
}

class FactoryAbstract {

    public static void main(String args[]) {

        System.out.println("\nAbstract Factory");

        //  TRANSPORTES = 1 Taxi, 2 Uber
        int tipo = 2;

        //  INTERFAZ FABRICA
        ITransporteFabrica fabrica = null;

        if(tipo == 1) {
            fabrica = new TaxiFabrica();
        }
        else if(tipo == 2) {
            fabrica = new UberFabrica();
        }

        ITransporte transporte = fabrica.crearTransporte();
        System.out.println(transporte.solicitar());
    }
}
