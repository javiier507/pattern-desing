package Creacionales;

interface DatabaseInterface {

    public boolean conectar();

    public int guardar(String datos);

    public String listar();
}

class SqlServer implements DatabaseInterface {

    public boolean conectar() {
        System.out.println("Conectando a Creacionales.SqlServer ...");
        return true;
    }

    public int guardar(String datos) {
        System.out.println("Guardando Datos de : " + datos);
        return 1;
    }

    public String listar() {
        System.out.println("Listando Datos ...");
        return "1";
    }
}

class MySql implements DatabaseInterface {

    public boolean conectar() {
        System.out.println("Conectando a MySQL ...");
        return true;
    }

    public int guardar(String datos) {
        System.out.println("Guardando Datos de : " + datos);
        return 1;
    }

    public String listar() {
        System.out.println("Listando Datos ...");
        return "1";
    }
}

abstract class DatabaseAbstract {

    public final String SQLServer = "sqlserver";
    public final String MySQL = "mysql";

    public abstract DatabaseInterface instanciar(String bd);
}

class DatabaseFactory extends DatabaseAbstract {

    public  DatabaseInterface instanciar(String bd) {

        DatabaseInterface instancia;

        switch (bd) {
            case  SQLServer:
                instancia = new SqlServer();
                break;
            case MySQL:
                instancia = new MySql();
                break;
            default:
                instancia = null;
                break;
        }

        return instancia;
    }
}

class Factory {
    public static void main(String args[]) {

        System.out.println();

        DatabaseAbstract fabric = new DatabaseFactory();

        DatabaseInterface sqlserver = fabric.instanciar(fabric.SQLServer);
        sqlserver.conectar();
        sqlserver.guardar("SQL Server");
        sqlserver.listar();

        System.out.println();

        DatabaseInterface mysql = fabric.instanciar(fabric.MySQL);
        mysql.conectar();
        mysql.guardar("MySQL");
        mysql.listar();

        System.out.println("\nCreacionales.Factory Method");
    }
}
