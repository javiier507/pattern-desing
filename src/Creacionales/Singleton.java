package Creacionales;

class Singleton {

    private static Singleton instancia;

    private String mensaje;

    private Singleton() {

    }

    public static Singleton getInstancia() {
        if(instancia == null)
            instancia = new Singleton();

        return instancia;
    }

    //  MENSAJE

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

class SingletonMain {

    public static void main(String args[]) {
        System.out.println("Creacionales.Singleton");

        Singleton singleton1 = Singleton.getInstancia();
        singleton1.setMensaje("Instancia 1");
        System.out.println("1 : " + singleton1.getMensaje());

        Singleton singleton2 = Singleton.getInstancia();
        System.out.println("2 : " + singleton2.getMensaje());

        singleton1.setMensaje("Instancia 2");
        System.out.println("1 : " + singleton1.getMensaje());
        System.out.println("2 : " + singleton2.getMensaje());
    }
}
