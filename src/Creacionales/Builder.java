package Creacionales;

//  https://es.wikipedia.org/wiki/Builder_(patr%C3%B3n_de_dise%C3%B1o)

//  PRODUCTO

class Pizza {
    private String masa;
    private String salsa;
    private String relleno;

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }

    public String getSalsa() {
        return salsa;
    }

    public void setSalsa(String salsa) {
        this.salsa = salsa;
    }

    public String getRelleno() {
        return relleno;
    }

    public void setRelleno(String relleno) {
        this.relleno = relleno;
    }
}

//  BUILDER

abstract class PizzaBuilder {

    protected Pizza pizza;

    public Pizza getPizza() {
        return pizza;
    }

    public abstract void buildMasa();
    public abstract void buildSalsa();
    public abstract void buildRelleno();

    public void crearNuevaPizza() {
        pizza = new Pizza();
        buildMasa();
        buildSalsa();
        buildRelleno();
    }
}

//  CONCRETE BUILDERS

class HawaiPizzaBuilder extends PizzaBuilder {

    public HawaiPizzaBuilder() {
        super.pizza = new Pizza();
    }

    @Override
    public void buildMasa() {
        pizza.setMasa("suave");
    }

    @Override
    public void buildSalsa() {
        pizza.setSalsa("dulce");
    }

    @Override
    public void buildRelleno() {
        pizza.setRelleno("chorizo+alcachofas");
    }
}

class PicantePizzaBuilder extends PizzaBuilder {

    public PicantePizzaBuilder() {
        //  super.pizza = new Pizza();
    }

    @Override
    public void buildMasa() {
        pizza.setMasa("cocida");
    }

    @Override
    public void buildSalsa() {
        pizza.setSalsa("picante");
    }

    @Override
    public void buildRelleno() {
        pizza.setRelleno("pimienta+salchichón");
    }
}

//  DIRECTOR

class PizzaDirector {

    private PizzaBuilder pizzaBuilder;

    public void setPizzaBuilder(PizzaBuilder pizzaBuilder) {
        this.pizzaBuilder = pizzaBuilder;
    }

    public void construirPizza() {
        /*pizzaBuilder.buildMasa();
        pizzaBuilder.buildSalsa();
        pizzaBuilder.buildRelleno();*/

        pizzaBuilder.crearNuevaPizza();
    }

    public Pizza getPizza() {
        return pizzaBuilder.getPizza();
    }
}

class Builder {
    public static void main(String args[]) {
        System.out.println("\n Builder Pattern");

        //  PRODUCT AND DIRECTOR
        Pizza pizza = null;
        PizzaDirector pizzaDirector = new PizzaDirector();

        //  HAWAI
        PizzaBuilder hawai = new HawaiPizzaBuilder();
        pizzaDirector.setPizzaBuilder(hawai);
        pizzaDirector.construirPizza();
        pizza = pizzaDirector.getPizza();

        //  PICANTE
        PizzaBuilder picante = new PicantePizzaBuilder();
        pizzaDirector.setPizzaBuilder(picante);
        pizzaDirector.construirPizza();
        pizza = pizzaDirector.getPizza();
    }
}
