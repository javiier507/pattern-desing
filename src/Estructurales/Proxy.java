package Estructurales;

//  https://danielggarcia.wordpress.com/2014/04/07/patrones-estructurales-vii-patron-proxy/
//  http://migranitodejava.blogspot.com/search/label/Proxy

//  Proporcionar un sustituto o intermediario para otro objeto de modo que pueda
//  controlarse el acceso que se tiene hacia él

//  SUJETO REAL

import java.util.ArrayList;

class ConnectionManager {

    private static boolean connection;

    public ConnectionManager() {
        connection = false;
    }

    public static void conectar() {
        connection = true;
    }

    public static void desconectar() {
        connection = false;
    }

    public static boolean getConnection() {
        return connection;
    }
}

//  SUJETO

interface IGuardar {
    void save(ArrayList datos);
}

class GuardarDisco implements IGuardar {

    public void save(ArrayList datos) {
        System.out.println("Guardando datos en disco ...");
    }
}

class GuardarRemoto implements IGuardar {

    public void save(ArrayList datos) {
        System.out.println("Guardando datos en Remoto ...");
    }
}

//  PROXY

class GuardarDatos implements IGuardar {

    public void save(ArrayList datos) {
        if(ConnectionManager.getConnection())
            new GuardarRemoto().save(datos);
        else
            new GuardarDisco().save(datos);
    }
}

//  MAIN

class Proxy {
    public static void main(String args[]) {
        System.out.println("\nProxy\n");

        GuardarDatos guardarDatos = new GuardarDatos();

        ArrayList<String> datos = new ArrayList<String>();
        datos.add("Real Madrid");
        datos.add("Liverpool");
        datos.add("Bayern Munich");
        datos.add("Roma");

        //  REMOTO
        ConnectionManager.conectar();
        guardarDatos.save(datos);

        //  DISCO
        ConnectionManager.desconectar();
        guardarDatos.save(datos);
    }
}
