package Estructurales;

//  http://codejavu.blogspot.com/2013/08/ejemplo-patron-adapter.html
//  https://es.wikipedia.org/wiki/Adaptador_(patr%C3%B3n_de_dise%C3%B1o)

abstract class Motor {
    abstract public void encender();
}

class Diesel extends Motor {

    public Diesel() {
        super();
        System.out.println("Motor Diesel");
    }

    @Override
    public void encender() {
        System.out.println("Encendiendo Motor Diesel");
    }
}

class Gasolina extends Motor {

    public Gasolina() {
        super();
        System.out.println("Motor Gasolina");
    }

    @Override
    public void encender() {
        System.out.println("Encendiendo Motor Gasolina");
    }
}

//  ADAPTABLE

class Electrico {

    public Electrico() {
        System.out.println("Motor Electrico");
    }

    public void recargar() {
        System.out.println("Recargando Motor Electrico");
    }
}

//  ADAPTADOR

class ElectricoAdapter extends Motor {

    private Electrico electrico;

    public ElectricoAdapter() {
        super();
        this.electrico = new Electrico();
        System.out.println("Motor Electrico Adapter");
    }

    @Override
    public void encender() {
        System.out.println("Encendiendo Motor Electrico Adapter");
    }
}

// MAIN

class Adapter {
    public static void main(String args[]) {
        System.out.println("\nPatron Adaptador\n");

        Motor diesel = new Diesel();
        diesel.encender();

        Motor gasolina = new Gasolina();
        gasolina.encender();

        System.out.println();

        Motor electrico = new ElectricoAdapter();
        electrico.encender();
    }
}
