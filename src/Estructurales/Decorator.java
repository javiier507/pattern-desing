package Estructurales;

//  http://migranitodejava.blogspot.com/2011/06/decorator.html

//  INTERFAZ DEL COMPONENTE

interface IAuto {

    String obtenerDescripcion();

    double obtenerPrecio();
}

//  COMPONENTE

abstract class Auto implements IAuto {

}

//  COMPONENTES CONCRETOS

class Hyundai extends Auto {

    public String obtenerDescripcion() {
        return "Hyundai";
    }

    public double obtenerPrecio() {
        return 22000;
    }
}

class Suzuki extends Auto {

    public String obtenerDescripcion() {
        return "Suzuki";
    }

    public double obtenerPrecio() {
        return 21000;
    }
}

//  DECORADOR

abstract class AutoDecorator implements IAuto {

    private IAuto iAuto;

    public AutoDecorator(IAuto iAuto) {
        setiAuto(iAuto);
    }

    public IAuto getiAuto() {
        return iAuto;
    }

    public void setiAuto(IAuto iAuto) {
        this.iAuto = iAuto;
    }
}

//  DECORADORES CONCRETOS

class Automatico extends AutoDecorator {

    public Automatico(IAuto iAuto) {
        super(iAuto);
    }

    public String obtenerDescripcion() {
        return getiAuto().obtenerDescripcion() + " Automatico";
    }

    public double obtenerPrecio() {
        return getiAuto().obtenerPrecio() + 1000;
    }
}

class SunRoof extends AutoDecorator {

    public SunRoof(IAuto iAuto) {
        super(iAuto);
    }

    public String obtenerDescripcion() {
        return getiAuto().obtenerDescripcion() + " SunRoof";
    }

    public double obtenerPrecio() {
        return getiAuto().obtenerPrecio() + 2000;
    }
}

class Decorator {
    public static void main(String args[]) {
        System.out.println("\nDecorator\n");

        IAuto hyundai = new Hyundai();
        hyundai = new Automatico(hyundai);
        System.out.println(hyundai.obtenerDescripcion());
        System.out.println("Precio : "+hyundai.obtenerPrecio());

        System.out.println();

        IAuto suzuki = new Suzuki();
        suzuki = new Automatico(suzuki);
        suzuki = new SunRoof(suzuki);
        System.out.println(suzuki.obtenerDescripcion());
        System.out.println("Precio : "+suzuki.obtenerPrecio());
    }
}
