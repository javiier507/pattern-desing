package Estructurales;

//  **
//  http://migranitodejava.blogspot.com/search/label/Composite
//  https://es.wikipedia.org/wiki/Composite_(patr%C3%B3n_de_dise%C3%B1o)

/*
    El patrón Composite sirve para construir objetos complejos a partir de otros más simples
     y similares entre sí, gracias a la composición recursiva y a una estructura en forma de árbol.
 */

//  COMPONENTE

import java.util.ArrayList;

interface ISalario {
    public double calcular();
}

//  COMPOSITE

class CompositeSalario implements   ISalario {

    private ArrayList<ISalario> empleados = new ArrayList<ISalario>();

    @Override
    public double calcular() {
        double suma = 0;
        for(int i=0; i<empleados.size(); i++) {
            suma += empleados.get(i).calcular();
        }
        return suma;
    }

    //  CRUD

    public void agregar(ISalario iSalario) {
        empleados.add(iSalario);
    }

    public  void eliminar(ISalario iSalario) {
        empleados.remove(iSalario);
    }
}

//  CHILDREN OF COMPOSITE

class Empresa extends CompositeSalario {
    public Empresa() {
        System.out.println("Sector Empresa");
    }
}

class Ventas extends CompositeSalario {
    public Ventas() {
        System.out.println("Sector Ventas");
    }
}

class Tecnologia extends CompositeSalario {
    public Tecnologia() {
        System.out.println("Sector Tecnologia");
    }
}

class Gerencia extends CompositeSalario {
    public Gerencia() {
        System.out.println("Sector Gerencia");
    }
}

//  HOJAS

class Empleado implements ISalario {

    private String nombre;
    private double salario;

    public Empleado(String nombre, double salario) {
        this.nombre = nombre;
        this.salario = salario;
    }

    @Override
    public double calcular() {
        return this.salario;
    }
}

//  CLIENTE

class Composite {
    public static void main(String args[]) {
        System.out.println("\nCOMPOSITE PATTERN\n");

        Empresa empresa = new Empresa();

        Ventas ventas = new Ventas();
        Tecnologia tecnologia = new Tecnologia();
        Gerencia gerencia = new Gerencia();

        empresa.agregar(ventas);
        ventas.agregar(new Empleado("Carlos", 900d));

        Empleado analista1 = new Empleado("Karen", 1400d);
        Empleado analista2 = new Empleado("Robert", 2000d);
        Empleado contador = new Empleado("Luisa", 1700d);
        Empleado gerente = new Empleado("Alberto", 2800);

        tecnologia.agregar(analista1);
        tecnologia.agregar(analista2);
        gerencia.agregar(contador);
        gerencia.agregar(gerente);

        empresa.agregar(tecnologia);
        empresa.agregar(gerencia);

        System.out.println("\nSUMA DE SALARIOS : " + empresa.calcular());

        tecnologia.eliminar(analista2);

        System.out.println("\nSUMA DE SALARIOS SIN ANALISTA 2 : " + empresa.calcular());
    }
}
