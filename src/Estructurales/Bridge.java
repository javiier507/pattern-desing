package Estructurales;

//  https://es.wikipedia.org/wiki/Bridge_(patr%C3%B3n_de_dise%C3%B1o)
//  http://migranitodejava.blogspot.com/search/label/Bridge
//  https://danielggarcia.wordpress.com/2014/03/17/patrones-estructurales-iv-patron-bridge/

//  Desacoplar una abstracción de su implementación de modo
//  que los dos puedan ser modificados de forma independiente

//  IMPLEMENTADOR

interface Publicacion {
    void publicarCorreo(String contenido);
    void publicarRedesSociales(String contenido);
}

//  IMPLEMENTADORES CONCRETOS

class Texto implements Publicacion {

    public void publicarCorreo(String contenido) {
        System.out.println("Publicando Texto Por Correo : " + contenido);
    }

    public void publicarRedesSociales(String contenido) {
        System.out.println("Publicando Texto Por Redes Sociales : " + contenido);
    }
}

class Imagen implements Publicacion {

    public void publicarCorreo(String contenido) {
        System.out.println("Publicando Imagen Por Correo : " + contenido);
    }

    public void publicarRedesSociales(String contenido) {
        System.out.println("Publicando Imagen Por Redes Sociales : " + contenido);
    }
}

//  ABSTRACCION

abstract class Medio {

    private Publicacion publicacion;

    public Medio(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    public abstract void publicar();

    public void publicarCorreo(String contenido) {
        publicacion.publicarCorreo(contenido);
    }

    public void publicarRedesSociales(String contenido) {
        publicacion.publicarRedesSociales(contenido);
    }
}

//  ABSTRACCIONES CONCRETAS

class Correo extends Medio {

    private String contenido;

    public Correo(Publicacion publicacion, String contenido) {
        super(publicacion);
        this.contenido = contenido;
    }

    public void publicar() {
        this.publicarCorreo(this.contenido);
    }
}

class RedesSociales extends Medio {

    private String contenido;

    public RedesSociales(Publicacion publicacion, String contenido) {
        super(publicacion);
        this.contenido = contenido;
    }

    public void publicar() {
        this.publicarRedesSociales(this.contenido);
    }
}

//  MAIN

class Bridge {
    public static void main(String args[]) {
        System.out.println("\nBridge\n");

        //  CORREOS
        Correo correo;

        correo = new Correo(new Texto(), "Buen Dias");
        correo.publicar();
        correo = new Correo(new Imagen(), "Buenos-Dias.jpg");
        correo.publicar();

        System.out.println();

        //  REDES SOCIALES

        RedesSociales redesSociales;

        redesSociales = new RedesSociales(new Texto(), "Buenos Dias");
        redesSociales.publicar();
        redesSociales = new RedesSociales(new Imagen(), "Buenos-Dias.pg");
        redesSociales.publicar();
    }
}
