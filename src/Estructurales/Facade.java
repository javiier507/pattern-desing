package Estructurales;

//  ESTE PATRON ES MUY FACIL

//  http://migranitodejava.blogspot.com/search/label/Facade
//  https://es.wikipedia.org/wiki/Facade_(patr%C3%B3n_de_dise%C3%B1o)

class ApiMercado {

    private String usuario;
    private String producto;

    public void conectar() {
        System.out.println("Conectando a la base de datos ");
    }

    public void ingresarDatos(String usuario, String producto) {
        this.usuario = usuario;
        this.producto = producto;
        System.out.println("Recibiendo Datos ...");
    }

    public String obtenerDatos() {
        return ("Datos Ingresados : " + this.usuario + " ; " + this.producto);
    }

    //  MUCHOS MAS METODOS ABAJO QUE HACEN ESTA CLASE MAS COMPLEJA
}

class ApiMercadoFacade {

    public void consultar(String usuario, String producto) {
        ApiMercado apiMercado = new ApiMercado();
        apiMercado.conectar();
        apiMercado.ingresarDatos(usuario, producto);
        System.out.println(apiMercado.obtenerDatos());
    }
}

class Facade {
    public static void main(String args[]) {
        System.out.println("\nFacade\n");

        ApiMercado apiMercado = new ApiMercado();
        apiMercado.conectar();
        apiMercado.ingresarDatos("carlos", "ipad");
        System.out.println(apiMercado.obtenerDatos());

        System.out.println();

        ApiMercadoFacade apiMercadoFacade = new ApiMercadoFacade();
        apiMercadoFacade.consultar("Carlos", "razer blande");
    }
}
